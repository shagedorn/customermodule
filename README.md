# CustomerModule: Description & copyright/warranty notice #

## INFO ##

**Note:** This module is not useful without the main project: 
https://bitbucket.org/shagedorn/modular-erp-app  
Documentation on this module can be found in the main project, too.

This project demonstrates the development of a modular iOS application, starting from a
typical single-project app which can be found in the 'T1 ERP App' repository. 
The development process is described in an assignment thesis which was written at 
TU Dresden/SALT Solutions in 2012.
  
**Runs on:** iOS 6 (iPad only)  
**IDE used:** Xcode 4.5

## LEGAL ##

### COPYRIGHT & WARRANTY NOTICE (Modular ERP App) ###

Modular ERP App was written by Sebastian Hagedorn as part of an assignment thesis at
TU Dresden/SALT Solutions GmbH. It was published under the following license (FreeBSD):

> Copyright (c) 2012 Sebastian Hagedorn
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
> 
> 1. Redistributions of source code must retain the above copyright notice, this
> list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice, this
> list of conditions and the following disclaimer in the documentation and/or other
> materials provided with the distribution.
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
> BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
> OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
> THE POSSIBILITY OF SUCH DAMAGE.
