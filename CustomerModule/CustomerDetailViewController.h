//
//  CustomerDetailViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"

/**
 *  This controller displays a customer's information.
 *
 *  To be displayed in a popover view controller.
 */
@interface CustomerDetailViewController : UIViewController

/**
 *  Always use this initialiser.
 *
 *  @param customer The customer whose information will be displayed.
 *  @return The initialised controller.
 */
- (id) initWithCustomer:(Customer*) customer;

@end
