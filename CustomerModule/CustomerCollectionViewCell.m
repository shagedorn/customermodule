//
//  CustomerCollectionViewCell.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CustomerCollectionViewCell.h"
#import "CustomerAddress.h"

#import <InfrastructureModule/BundleResourceHelperClass.h>

#define SELECTED_BACKGROUND_VIEW_TAG 230859674543

@interface CustomerCollectionViewCell ()

///==========================================
/** Private Interface */
///==========================================
/**
 *  The title lable, filled with the customer's name.
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/**
 *  The image view displays a status image.
 *
 *  Green indicates a valid address. Grey indicates an invalid address.
 */
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;

@end

@implementation CustomerCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {

        NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"CustomerModuleBundle"];
        NSArray *arrayOfViews = [myBundle loadNibNamed:@"CustomerCollectionViewCell" owner:self options:nil];
        if ([arrayOfViews count] < 1) {
            return nil;
        }
    
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
        self.layer.cornerRadius = 15;
        
        UIView *selBackgroundView = [self viewWithTag:SELECTED_BACKGROUND_VIEW_TAG];
        [selBackgroundView removeConstraints:selBackgroundView.constraints];
        self.selectedBackgroundView = selBackgroundView;
        selBackgroundView.hidden = NO;
    }
    return self;
    
}

- (void)displayCustomer:(Customer *)customer {
    self.titleLabel.text = customer.name;

    NSString *imagePath;
    if ([customer.address isValid]) {
        imagePath = [BundleResourceHelperClass pathForResource:@"address_valid"
                                                        ofType:@"png"
                                                      inModule:@"CustomerModuleBundle"];
        self.statusImage.image = [UIImage imageWithContentsOfFile:imagePath];
    } else {
        imagePath = [BundleResourceHelperClass pathForResource:@"address_not_valid"
                                                        ofType:@"png"
                                                      inModule:@"CustomerModuleBundle"];
        self.statusImage.image = [UIImage imageWithContentsOfFile:imagePath];
    }
}

@end
