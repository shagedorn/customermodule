//
//  CustomerViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <InfrastructureModule/Module.h>

/**
 *  Displays all customers in a collection view. Shows selected customer
 *  in a popover.
 */
@interface CustomerViewController : UIViewController <Module, UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate>

@end
