//
//  CustomerCollectionViewCell.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 8/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"

/**
 *  Cell to display customer information in a collection view (grid).
 */
@interface CustomerCollectionViewCell : UICollectionViewCell

/**
 *  Update customer information.
 *
 *  @param customer The Customer object to be displayed.
 */
- (void) displayCustomer:(Customer*)customer;

@end