//
//  Customer.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "Customer.h"
#import "CustomerAddress.h"

#import <InfrastructureModule/CoreDataAppDelegate.h>
#import <InfrastructureModule/CoreDataHelperClass.h>

@implementation Customer

@dynamic customerId;
@dynamic name;
@dynamic address;

#pragma mark - ServerEntity Protocol

- (void)updateWithInformation:(NSDictionary *)infoDict {
    
    BOOL hasChanged = NO;
    
    if (!self.customerId) {
        // Initial creation
        self.customerId = [NSNumber numberWithInteger:[[infoDict objectForKey:@"id"] integerValue]];
        DLog(@"New customer: %@", self.customerId);
    }
    
    NSString *newName = [infoDict objectForKey:@"name"];
    if (newName && ![self.name isEqualToString:newName]) {
        self.name = newName;
        hasChanged = YES;
    }
    
    NSString *addressIdString = [infoDict objectForKey:@"address"];
    if (addressIdString && (addressIdString.integerValue != self.address.addressId.integerValue)) {
        id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
        CustomerAddress *address = (CustomerAddress*)[CoreDataHelperClass checkForExistingEntity:@"CustomerAddress"
                                                             usingKeyAttribute:@"addressId"
                                                                     withValue:[NSNumber numberWithInteger:addressIdString.integerValue]
                                                                     inContext:[appDelegate managedObjectContext]];
        if (address) {
            self.address = address;
        } else {
            DLog(@"Address with ID '%d' not found.", addressIdString.integerValue);
        }
        hasChanged = YES;
    }

    if (hasChanged) {
        DLog(@"Customer '%@' has changed.", self.name);
    }
}

@end
