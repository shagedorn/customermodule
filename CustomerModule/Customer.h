//
//  Customer.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import <InfrastructureModule/ServerEntity.h>

@class CustomerAddress;

/**
 *  Represents a real-world customer.
 */
@interface Customer : NSManagedObject <ServerEntity>

/**
 *  A unique ID.
 */
@property (nonatomic, retain) NSNumber * customerId;

/**
 *  The customer's name.
 */
@property (nonatomic, retain) NSString * name;

/**
 *  The customer's address.
 */
@property (nonatomic, retain) CustomerAddress *address;

@end