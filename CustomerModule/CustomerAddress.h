//
//  CustomerAddress.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import <InfrastructureModule/ServerEntity.h>

@class Customer;

/**
 *  An address which is associated with a customer.
 */
@interface CustomerAddress : NSManagedObject <ServerEntity>

/**
 *  A unique ID.
 */
@property (nonatomic, retain) NSNumber * addressId;

/**
 *  The country.
 */
@property (nonatomic, retain) NSString * country;

/**
 *  The street.
 */
@property (nonatomic, retain) NSString * street;

/**
 *  The street number.
 */
@property (nonatomic, retain) NSNumber * streetNumber;

/**
 *  All customers with this address. Only set via the
 *  inverse relation.
 */
@property (nonatomic, retain) NSSet *customers;

/**
 *  An address is considered valid when country, street
 *  and street number have been provided.
 */
- (BOOL) isValid;

@end

@interface CustomerAddress (CoreDataGeneratedAccessors)

- (void)addCustomersObject:(Customer *)value;
- (void)removeCustomersObject:(Customer *)value;
- (void)addCustomers:(NSSet *)values;
- (void)removeCustomers:(NSSet *)values;

@end
