//
//  CustomerViewController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "CustomerViewController.h"
#import "CustomerDetailViewController.h"
#import "CustomerCollectionViewCell.h"
#import "Customer.h"
#import "CustomerAddress.h"

#import <InfrastructureModule/CoreDataAppDelegate.h>
#import <InfrastructureModule/CoreDataHelperClass.h>
#import <InfrastructureModule/ServerEntity.h>
#import <InfrastructureModule/BundleResourceHelperClass.h>

#import "AFNetworking.h"

#pragma mark - Private Interface

@interface CustomerViewController ()

#pragma mark - IB Outlets

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UIProgressView *loadingProgessBar;

#pragma mark - Private Properties

@property (nonatomic, copy) UpdateBlock updateBlock;
@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, strong) NSOperationQueue *customerLoadingQueue;
@property (nonatomic, strong) NSFetchedResultsController *customerResultsController;
@property (nonatomic, strong) UIPopoverController *currentPopover;
@property (nonatomic, readonly) NSString *bundleName;
@property (nonatomic, strong) NSDictionary *moduleInfo;

#pragma mark - Private methods

///==========================================
/** User actions */
///==========================================
/**
 *  Called when the user clicks the 'x' button.
 *
 *  It releases the view and
 *  calls the closeModuleBlock.
 *
 *  @param sender The close button.
 */
- (void) close:(id)sender;

/**
 *  Triggers loading customer data.
 *
 *  @param sender The load button.
 */
- (void) load:(id)sender;

///==========================================
/** Data loading */
///==========================================
/**
 *  Loading data was successfull.
 *
 *  @param response The data coming from the server.
 *  Assumed to be text.
 */
- (void) customerLoadingHasFinished:(id)response;
/**
 *  Loading data from the server failed. The error will be logged.
 *
 *  @param error An error description.
 */
- (void) customerLoadingHasBeenCancelledWithError:(NSError*)error;

@end

#pragma mark - Implementation

@implementation CustomerViewController {}

#pragma mark - Accessors

- (NSString *)bundleName {
    return [self.moduleInfo objectForKey:@"BundleName"];
}

- (NSOperationQueue *)customerLoadingQueue {
    if (!_customerLoadingQueue) {
        _customerLoadingQueue = [[NSOperationQueue alloc] init];
    }
    return _customerLoadingQueue;
}

#pragma mark - Module Protocol

@synthesize closeModuleBlock = _closeModuleBlock;

- (UIImage *)modulePreviewImage {
    NSString *imagePath = [BundleResourceHelperClass pathForResource:@"module_preview_customer"
                                                              ofType:@"png"
                                                            inModule:self.bundleName];
    return [UIImage imageWithContentsOfFile:imagePath];
}

- (UIViewController *)moduleLaunchController {
    return self;
}

- (NSArray *)moduleDependencies {
    return [self.moduleInfo objectForKey:@"ModuleDependencies"];
}

- (void)moduleLoadData:(NSManagedObjectContext *)context
 usingBlockForProgress:(UpdateBlock)updateBlock
         andCompletion:(CompletionBlock)completionBlock {

    // these may be nil
    self.updateBlock = updateBlock;
    self.completionBlock = completionBlock;
    
    // don't start another loading process...
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.loadingProgessBar.progress = 0.0;
 
    // Start server request
    NSString *plistPath = [BundleResourceHelperClass pathForResource:@"CustomerServerInfo"
                                                              ofType:@"plist"
                                                            inModule:self.bundleName];
    NSDictionary *plist = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    NSString *filename = [plist objectForKey:@"filename"];
    NSString *extensiom = [plist objectForKey:@"format"];
    NSString *serverURL = [[NSBundle mainBundle].infoDictionary objectForKey:@"ServerURL"];
    serverURL = [[serverURL stringByAppendingPathComponent:filename] stringByAppendingPathExtension:extensiom];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:serverURL]
                                                  cachePolicy:NSURLCacheStorageNotAllowed
                                              timeoutInterval:20];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    // On Updates...
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float ratio = (float)totalBytesRead/(float)totalBytesExpectedToRead;
        if (self.updateBlock) {
            self.updateBlock([NSNumber numberWithFloat:ratio], self);
        }
        self.loadingProgessBar.progress = ratio;
    }];
    
    // On completion or error
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (self.completionBlock) {
            self.completionBlock(nil, self);
        }
        [self customerLoadingHasFinished:responseObject];

        self.updateBlock = nil;
        self.completionBlock = nil;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (self.completionBlock) {
            self.completionBlock(error, self);
        }
        [self customerLoadingHasBeenCancelledWithError:error];
        
        self.updateBlock = nil;
        self.completionBlock = nil;
    }];
    
    // Fire request
    [self.customerLoadingQueue addOperation:operation];
}

- (NSString *)moduleDescription {
    return [self.moduleInfo objectForKey:@"ModuleDisplayName"];
}

#pragma mark - Lifecycle

- (id)init {
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"CustomerModuleBundle"];
    self = [super initWithNibName:@"CustomerViewController" bundle:myBundle];
    if (self) {
        // Load module information
        NSString *plistPath = [myBundle pathForResource:@"ModuleDescription" ofType:@"plist"];
        self.moduleInfo = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Register collection view cells
    [self.myCollectionView registerClass:[CustomerCollectionViewCell class] forCellWithReuseIdentifier:@"CustomerCell"];
    
    // Set title
    self.navigationItem.title = [self moduleDescription];
    
    // Set close button
    NSString *imagePath = [BundleResourceHelperClass pathForResource:@"close_button"
                                                              ofType:@"png"
                                                            inModule:@"InfrastructureModuleBundle"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage alloc] init]
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self
                                                                            action:@selector(close:)];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage imageWithContentsOfFile:imagePath]
                                                     forState:UIControlStateNormal
                                                   barMetrics:UIBarMetricsDefault];
    imagePath = [BundleResourceHelperClass pathForResource:@"close_button_pressed"
                                                    ofType:@"png"
                                                  inModule:@"InfrastructureModuleBundle"];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage imageWithContentsOfFile:imagePath]
                                                     forState:UIControlStateHighlighted
                                                   barMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *startLoadingItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(load:)];
    self.navigationItem.rightBarButtonItem = startLoadingItem;
    
    // inactive
    self.loadingProgessBar.alpha = 0.5;
    
    // FetchedResultsController for collection view content
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    NSSortDescriptor *sorting = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"Customer"];
    req.sortDescriptors = @[sorting];
    self.customerResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:req
                                                                         managedObjectContext:[appDelegate managedObjectContext]
                                                                           sectionNameKeyPath:nil
                                                                                    cacheName:nil];
    self.customerResultsController.delegate = self;
    [self.customerResultsController performFetch:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.customerLoadingQueue cancelAllOperations];
}

#pragma mark - User actions

- (void) close:(id)sender {
    self.closeModuleBlock(^{
        // unload view when module is loaded but not showing
        self.view = nil;
    });
}

- (void)load:(id)sender {
    self.loadingProgessBar.alpha = 1.0;
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    [self moduleLoadData:[appDelegate managedObjectContext]
   usingBlockForProgress:nil
           andCompletion:nil];
}

#pragma mark - Data loading

- (void)customerLoadingHasFinished:(id)response {
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.navigationItem.rightBarButtonItem.enabled = YES;
                         self.loadingProgessBar.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {}];
    
    NSString *plistString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    NSDictionary *plist = [plistString propertyList];
    
    NSArray *customers = [plist objectForKey:@"customers"];
    NSArray *addresses = [plist objectForKey:@"addresses"];
    
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    
    // Load addresses first
    NSError *error;
    error = [CoreDataHelperClass saveOrUpdateAllServerEntitiesOfType:[CustomerAddress class]
                                    withPrimaryDictionaryKey:@"id"
                                     andPrimaryAttributeName:@"addressId"
                                                  fromSource:addresses
                                                        into:[appDelegate managedObjectContext]];
    
    if (error) {
        DLog(@"Address Error: %@", error);
        error = nil;
    }
    
    // Next: customers
    error = [CoreDataHelperClass saveOrUpdateAllServerEntitiesOfType:[Customer class]
                                    withPrimaryDictionaryKey:@"id"
                                     andPrimaryAttributeName:@"customerId"
                                                  fromSource:customers
                                                        into:[appDelegate managedObjectContext]];
    
    if (error) {
        DLog(@"Customer Error: %@", error);
    }
    
    // Done...
}

- (void)customerLoadingHasBeenCancelledWithError:(NSError *)error {
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.navigationItem.rightBarButtonItem.enabled = YES;
                         self.loadingProgessBar.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {}];
    DLog(@"Customer loading error: %@", error);
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.customerResultsController.sections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.customerResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifier = @"CustomerCell";
    CustomerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Customer *currentCustomer = [self.customerResultsController objectAtIndexPath:indexPath];
    [cell displayCustomer:currentCustomer];
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPopover) {
        [self.currentPopover dismissPopoverAnimated:YES];
    }
    
    Customer *selectedCustomer = [self.customerResultsController objectAtIndexPath:indexPath];
    
    self.currentPopover = [[UIPopoverController alloc] initWithContentViewController:
                           [[CustomerDetailViewController alloc] initWithCustomer:selectedCustomer]];
    CustomerCollectionViewCell *cell = (CustomerCollectionViewCell*)[self collectionView:collectionView cellForItemAtIndexPath:indexPath];
    [self.currentPopover setPopoverContentSize:CGSizeMake(200, 300) animated:YES];
    [self.currentPopover presentPopoverFromRect:cell.frame
                                         inView:self.myCollectionView
                       permittedArrowDirections:UIPopoverArrowDirectionAny
                                       animated:YES];
}

#pragma mark - FetchedResultsController Delegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    static float duration = 0.5;
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.myCollectionView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.myCollectionView reloadData];
                         [UIView animateWithDuration:duration
                                               delay:0.2
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              self.myCollectionView.alpha = 1.0;
                                          } completion:nil];
                     }];
}


@end
